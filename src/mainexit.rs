use std::sync::Arc;

use manage::{ManageReadlineStatus, SiminkManage};
use rcu::drain_call_rcu;
use tracelog::tracelog_exit;

/// 主程序进入退出流程
pub fn main_exit(manage: Arc<SiminkManage>) {
    manage.set_simink_quit();

    tracelog_exit();

    drain_call_rcu();

    manage.worker_join();
    // Debug
    println!("simink:   {:?}", manage.simink_status());
    if manage.readline_status() == ManageReadlineStatus::Interrupted {
        println!("Exit on Readline Interrupted");
    }
    println!("Exit reason: {:?}", manage.simink_quit_cause());
}
