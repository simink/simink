//! arm 平台 linux 启动特定接口实现

/// arm 平台 linux 启动特定接口实现
pub trait ArmLinuxBoot {
    /// 配置设备以直接引导 ARM Linux 内核(设备重置将其置于内核在固件初始化后期望的状态,
    /// 而不是真正的硬件重置状态)。这个函数在 board 构造完成后被调用一次(在第一次系统重置之前)
    fn arm_linux_init(&self, secure_boot: bool);
}
