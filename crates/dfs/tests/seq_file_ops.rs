use std::sync::Arc;

use dfs::{
    inode::inode_root,
    seq_file::{SeqFileCreateOptions, SeqFileOperation},
    seq_println, DfsOpenOptions, DfsResult,
};

struct TestSeqFile;

impl SeqFileOperation for TestSeqFile {
    fn show(
        &self,
        m: &mut dfs::seq_file::SeqFile,
        _: &mut Box<dyn std::any::Any + Send>,
    ) -> DfsResult<()> {
        seq_println!(m, "Hello world");
        seq_println!(m, "this is seq file test");
        Ok(())
    }
}

#[test]
fn seq_file_create() {
    SeqFileCreateOptions::new()
        .name("test1")
        .read_write(true)
        .create_seq_file(inode_root(), Arc::new(TestSeqFile))
        .unwrap();

    let file = DfsOpenOptions::new().read_write(true).open("/test1").unwrap();
    let mut buf = String::with_capacity(2);
    let _ = file.read(&mut buf).unwrap();
    assert_eq!(buf.as_str(), "He");
    let mut buf1 = String::new();
    let _ = file.read(&mut buf1).unwrap();
    assert_eq!(buf1.as_str(), "llo world\nthis is seq file test\n");
}
