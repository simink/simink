//! arm gicv2 硬件实现
#![feature(lazy_cell)]
#![allow(unused)]
#![allow(non_upper_case_globals)]
#![allow(clippy::cast_lossless)]
#![allow(clippy::cast_possible_truncation)]
#![allow(clippy::cast_sign_loss)]
#![allow(clippy::cast_precision_loss)]
#![allow(clippy::unused_self)]
#![allow(clippy::cast_possible_wrap)]

mod gicv2;
mod gicv2m;

pub use gicv2::*;
pub use gicv2m::*;
