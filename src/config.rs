use std::{
    fs::OpenOptions,
    io::{self, BufRead},
    path::PathBuf,
    sync::Arc,
};

use monitor::Monitor;

use crate::cli::SiminkCli;

pub(crate) fn config_init(mon: Arc<Monitor>, arg: &SiminkCli) {
    if let Some(config) = arg.config() {
        config_process(mon, config);
    }
}

fn config_process(mon: Arc<Monitor>, config: PathBuf) {
    let file = OpenOptions::new().read(true).open(config).unwrap();
    let lines = io::BufReader::new(file).lines();
    for line in lines {
        if line.as_ref().unwrap().starts_with('#') {
            continue;
        }
        mon.readline_exec(&line.unwrap(), None).map_err(|e| println!("{}", e)).unwrap();
    }
}
