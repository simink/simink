use std::sync::Arc;

use dfs::inode::{inode_root, DirCreateOptions};
use manage::SiminkManage;

use self::{current_board::current_board_init, status::status_board_init};

mod current_board;
mod status;

pub(crate) fn board_init(manage: Arc<SiminkManage>) {
    let board_inode =
        DirCreateOptions::new().read_write(true).name("board").create_dir(inode_root()).unwrap();

    current_board_init(manage.clone(), board_inode.clone());
    status_board_init(manage, board_inode);
}
