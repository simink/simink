use arm_linux_boot::ArmLinuxBoot;
use objectid::ObjectId;

use crate::GICv2State;

impl ObjectId for GICv2State {
    fn compatible(&self) -> &'static str {
        "arm_gic"
    }

    fn id(&self) -> &str {
        &self.name
    }
}

impl ArmLinuxBoot for GICv2State {
    fn arm_linux_init(&self, secure_boot: bool) {
        let mut inner = self.inner.lock().unwrap();
        if inner.security_extn && !secure_boot {
            // 直接将内核引导到非安全状态. 如果这个 GIC 实现了安全拓展, 那么我们必须将其配置为所有中断都是非安全的
            // (这是一项由实际硬件中的安全固件完成的工作, 在这种模式下, simink 充当了一个极简的固件和引导加载程序等同物)
            inner.irq_reset_nonsecure = true;
        }
    }
}
