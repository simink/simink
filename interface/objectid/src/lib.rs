//! 对象标识实现

/// 对象标识实现
pub trait ObjectId {
    /// 对象兼容标识名称
    fn compatible(&self) -> &'static str;
    /// 对象全局唯一标识符
    fn id(&self) -> &str;
}
