use std::sync::Arc;

use dfs::command::dfs_monitor_registers;
use manage::SiminkManage;
use monitor::Monitor;

use self::{
    boardinfo::monitor_command_boardinfo_register,
    clear::monitor_command_clear_register,
    exit::{
        monitor_command_exit_register, monitor_command_quit_rl_register,
        monitor_command_run_rl_register,
    },
    history::monitor_command_history_register,
};

mod boardinfo;
mod clear;
mod exit;
mod history;

pub(crate) fn simink_monitor_registers(mon: Arc<Monitor>, manage: Arc<SiminkManage>) {
    // 注册外部的 dfs 监视器命令
    dfs_monitor_registers(&mon);

    monitor_command_exit_register(&mon, manage.clone());
    monitor_command_quit_rl_register(&mon, manage.clone());
    monitor_command_run_rl_register(&mon, manage.clone());
    monitor_command_boardinfo_register(&mon);
    monitor_command_history_register(mon.clone());
    monitor_command_clear_register(mon);
}
