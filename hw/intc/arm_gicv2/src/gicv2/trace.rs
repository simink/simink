use tracelog::tracelog;

#[tracelog(arm_gic, gic_enable_irq, "irq {} enabled", 1, irq)]
#[tracelog(arm_gic, gic_disable_irq, "irq {} disabled", 1, irq)]
#[tracelog(
    arm_gic,
    gic_set_irq,
    "irq {} level {} cpumask 0x{:x} target 0x{:x}",
    4,
    irq,
    level,
    cpumask,
    target
)]
#[tracelog(
    arm_gic,
    gic_update_bestirq,
    "{} {} irq {} priority {} cpu priority mask {} cpu running priority {}",
    6,
    s,
    cpu,
    irq,
    prio,
    priority_mask,
    running_priority
)]
#[tracelog(arm_gic, gic_update_set_irq, "cpu[{}]: {} = {}", 3, cpu, s, level)]
#[tracelog(arm_gic, gic_acknowledge_irq, "{} {} acknowledged irq {}", 3, s, cpu, irq)]
#[tracelog(arm_gic, gic_cpu_write, "{} {} iface write at 0x{:08x} 0x{:08x}", 4, s, cpu, addr, val)]
#[tracelog(arm_gic, gic_cpu_read, "{} {} iface read at 0x{:08x}: 0x{:08x}", 4, s, cpu, addr, val)]
#[tracelog(arm_gic, gic_hyp_read, "hyp read at 0x{:08x}: 0x{:08x}", 2, addr, val)]
#[tracelog(arm_gic, gic_hyp_write, "hyp write at 0x{:08x}: 0x{:08x}", 2, addr, val)]
#[tracelog(arm_gic, gic_dist_read, "dist read at 0x{:08x} size {}: 0x{:08x}", 3, addr, size, val)]
#[tracelog(arm_gic, gic_dist_write, "dist write at 0x{:08x} size {}: 0x{:08x}", 3, addr, size, val)]
#[tracelog(arm_gic, gic_lr_entry, "cpu {}: new lr entry {}: 0x{:08x}", 3, cpu, entry, val)]
#[tracelog(arm_gic, gic_update_maintenance_irq, "cpu {}: maintenance = {}", 2, cpu, val)]
#[derive(Default)]
pub(crate) struct GICv2Trace {
    irq: usize,
    level: bool,
    cpumask: usize,
    target: usize,
    s: String,
    cpu: usize,
    prio: usize,
    priority_mask: usize,
    running_priority: usize,
    addr: usize,
    val: usize,
    size: usize,
    entry: usize,
}
