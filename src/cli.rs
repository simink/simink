use std::path::PathBuf;

use clap::Parser;

#[derive(Debug, Parser)]
#[command(author, version, about, long_about = None)]
pub(crate) struct SiminkCli {
    /// 进入交互式命令行
    #[arg(short, long, default_value = "false")]
    interactive: bool,

    /// 加载配置文件
    #[arg(short, long, value_name = "FILE")]
    config: Option<PathBuf>,

    /// 启用控制协议
    #[arg(short, long, default_value = "false")]
    protocol: bool,

    /// 控制协议地址
    #[arg(short, long, default_value = "127.0.0.1:1234")]
    tcp: String,
}

impl SiminkCli {
    pub(crate) fn config(&self) -> Option<PathBuf> {
        self.config.clone()
    }

    pub(crate) fn interactive(&self) -> bool {
        self.interactive
    }

    pub(crate) fn protocol(&self) -> bool {
        self.protocol
    }

    pub(crate) fn tcp(&self) -> String {
        self.tcp.clone()
    }
}

pub(crate) fn parse_cli() -> SiminkCli {
    SiminkCli::parse()
}
