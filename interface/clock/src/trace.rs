use tracelog::tracelog;

#[tracelog(hwclock, clock_set_source, "'{}', src='{}'", 2, clk_name, src_clk_name)]
#[tracelog(hwclock, clock_set, "'{}', {}Hz->{}Hz", 3, clk_name, old_hz, new_hz)]
#[tracelog(hwclock, clock_propagate, "'{}'", 1, clk_name)]
#[tracelog(
    hwclock,
    clock_update,
    "'{}', src='{}', val={}Hz notify={}",
    4,
    clk_name,
    src_clk_name,
    new_hz,
    notify
)]
#[tracelog(
    hwclock,
    clock_set_mul_div,
    "'{}', mul: {} -> {}, div: {} -> {}",
    5,
    clk_name,
    old_multiplier,
    new_multiplier,
    old_divider,
    new_divider
)]
#[derive(Default)]
pub(crate) struct HwClockTrace {
    clk_name: String,
    src_clk_name: String,
    old_hz: u64,
    new_hz: u64,
    notify: bool,
    old_multiplier: u64,
    old_divider: u64,
    new_multiplier: u64,
    new_divider: u64,
}
