use std::sync::Arc;

use chardev::{CharBackendDevice, CharBackendImpl};
use monitor::Monitor;
use objectid::ObjectId;

#[allow(unused)]
pub(crate) struct CharDeviceMon {
    mon: Arc<Monitor>,
    char_backend_device: Arc<CharBackendDevice>,
}

impl CharDeviceMon {
    pub(crate) fn create(mon: Arc<Monitor>, char_backend_device: Arc<CharBackendDevice>) -> Self {
        Self { mon, char_backend_device }
    }
}

impl CharBackendImpl for CharDeviceMon {
    fn backend_can_recv(&self) -> usize {
        1
    }

    fn backend_receive(&self, buf: &[u8]) {
        self.char_backend_device.chr_write(buf).unwrap();
        self.char_backend_device.chr_notify_accept_input();
    }

    fn backend_event(&self, _event: chardev::CharDeviceEvent) {}
}

impl ObjectId for CharDeviceMon {
    fn compatible(&self) -> &'static str {
        "monitor"
    }

    fn id(&self) -> &str {
        "monitor"
    }
}
