//! 提供内存屏障
use std::sync::atomic::{self, fence, Ordering};

/// 编译器内存屏障
///
/// 阻止当前位置前后读写编译器的重排序
#[inline(always)]
pub fn barrier() {
    atomic::compiler_fence(Ordering::SeqCst);
}

/// 处理器内存屏障
///
/// 根据指定的顺序, 防止编译器和处理器围绕它重新排序某些类型的内存操作,
/// 该函数具有最严格的屏障级别
#[inline(always)]
pub fn smp_mb() {
    barrier();
    fence(Ordering::SeqCst);
}

/// 处理器写内存屏障
///
/// 适用于写数据操作, 防止编译器和处理器重排序读写操作到该位置之后
#[inline(always)]
pub fn smp_mb_release() {
    barrier();
    fence(Ordering::Release);
}

/// 处理器读内存屏障
///
/// 适用于读数据操作, 防止编译器和处理器重排序读写操作到该位置之前
#[inline(always)]
pub fn smp_mb_acquire() {
    barrier();
    fence(Ordering::Acquire);
}

/// 处理器写内存屏障
///
/// 同 `smp_mb_release` 一致
#[inline(always)]
pub fn smp_wmb() {
    smp_mb_release();
}

/// 处理器读内存屏障
///
/// 同 `smp_mb_acquire` 一致
#[inline(always)]
pub fn smp_rmb() {
    smp_mb_acquire();
}

/// 处理器内存屏障
///
/// `smp_mb` 变体, 功能一致
#[inline(always)]
pub fn smp_mb_placeholder() {
    smp_mb();
}

/// 处理器内存屏障
///
/// `smp_mb` 变体, 功能一致
#[inline(always)]
pub fn smp_mb_global() {
    smp_mb();
}
