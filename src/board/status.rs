use std::sync::Arc;

use dfs::{
    inode::DfsInode,
    params::{ParamCreateOptions, ParamOperation},
};
use manage::SiminkManage;

struct Status {
    manage: Arc<SiminkManage>,
}

pub(crate) fn status_board_init(manage: Arc<SiminkManage>, boardinode: Arc<DfsInode>) {
    ParamCreateOptions::new()
        .read(true)
        .name("status")
        .create_param_file(boardinode, Arc::new(Status { manage }))
        .unwrap();
}

impl ParamOperation for Status {
    fn read(&self) -> String {
        self.manage.guest_status().name().to_string()
    }
}
