use tracelog::tracelog;

#[tracelog(pl011, pl011_irq_state, "irq state {}", 1, irq_level)]
#[tracelog(pl011, pl011_read, "addr 0x{:08x} value 0x{:08x}", 2, addr, value)]
#[tracelog(pl011, pl011_read_fifo, "FIFO read, read_count now {}", 1, read_count)]
#[tracelog(pl011, pl011_write, "addr 0x{:08x} value 0x{:08x}", 2, addr, value)]
#[tracelog(
    pl011,
    pl011_can_receive,
    "LCR 0{:08x} read_count {} returning {}",
    3,
    lcr,
    read_count,
    value
)]
#[tracelog(pl011, pl011_put_fifo, "new char 0x{:x} read_count now {}", 2, value, read_count)]
#[tracelog(pl011, pl011_put_fifo_full, "FIFO now full, RXFF set", 0)]
#[tracelog(
    pl011,
    pl011_baudrate_change,
    "new baudrate {} (clk: {} hz, ibrd: {}, fbrd: {}",
    4,
    baudrate,
    clock,
    ibrd,
    fbrd
)]
#[derive(Default)]
pub(crate) struct Pl011StateTrace {
    irq_level: bool,
    addr: u64,
    value: u64,
    read_count: i32,
    lcr: u32,
    baudrate: u64,
    clock: u64,
    ibrd: u32,
    fbrd: u32,
}
