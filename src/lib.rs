//! simink 控制
#![feature(lazy_cell)]
#![allow(clippy::unimplemented)]

mod board;
mod cli;
mod command;
mod config;
mod mainexit;
mod maininit;
mod mainloop;
mod monitor;

pub use mainexit::main_exit;
pub use maininit::main_init;
pub use mainloop::main_loop;
