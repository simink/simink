//! 提供通用的 cpu 管理接口
use std::cell::RefCell;

struct CPUState {
    cpu_index: i32,
}

thread_local! {
    static CURRENT_CPU: RefCell<CPUState> = RefCell::new(CPUState {cpu_index: -1});
}

/// 返回当前 cpu index
///
/// 如果当前线程不是 cpu 执行线程, 那么 index 为 -1
pub fn current_cpu_index() -> i32 {
    CURRENT_CPU.with(|cpu| cpu.borrow().cpu_index)
}

/// 设置当前 cpu 的 index
pub fn set_current_cpu_index(cpu: i32) {
    CURRENT_CPU.with(|this| {
        this.borrow_mut().cpu_index = cpu;
    });
}
