use bitops::{bitmap::BitMap, clear_bit, extract32, find_last_bit, set_bit};

#[test]
fn bitops_test() {
    let mut bit = [0b101010111101; 1];
    set_bit(1, &mut bit);
    assert_eq!(bit[0], 0b101010111111);
    clear_bit(5, &mut bit);
    assert_eq!(bit[0], 0b101010011111);

    let mut bit = [0b101010111101, 0b0000];
    set_bit(64, &mut bit);
    assert_eq!(bit[1], 1);
    let index = find_last_bit(&bit, 128);
    assert_eq!(index, 64);

    let bits = bitops::bit(0);
    assert_eq!(bits, 1);
    let bits = bitops::bit(31);
    assert_eq!(bits, 0x80000000);

    let bit_mask = bitops::bit_mask(64);
    assert_eq!(bit_mask, 1);

    let bit_word = bitops::bit_word(2);
    assert_eq!(bit_word, 0);

    let bit_word = bitops::bit_word(65);
    assert!(bit_word == 1 || bit_word == 2);

    let value = 0x8000008Eu32;
    let ex = extract32(value, 1, 3);
    assert_eq!(ex, 7);

    let mut bitmap = BitMap::create(4);
    bitmap.set_fill();
    assert_eq!(bitmap.count_one(), 4);
    bitmap.set_zero();
    assert_eq!(bitmap.count_one(), 0);

    bitmap.bitmap_set(0, 2);
    assert_eq!(bitmap.count_one(), 2);
}
