//! simink 仿真器
use simink::{main_exit, main_init, main_loop};

fn main() {
    main_exit(main_loop(main_init()));
}
