//! simink 管理
//!
//! 管理 simink 中核心部件的运行状态, 并维护同步状态
#![feature(thread_id_value)]

use std::{sync::Arc, thread::ThreadId};

use dfs::inode::DfsInode;
use guest::ManageGuest;

use self::{readline::ManageReadline, simink::ManageSimink, worker::ManageWorkerPool};

mod guest;
mod readline;
mod simink;
mod worker;

pub use guest::ManageGuestStatus;
pub use readline::ManageReadlineStatus;
pub use simink::{ManageSiminkStatus, SiminkQuitCause, SiminkRequest};

/// Simink 程序管理结构
pub struct SiminkManage {
    /// 工作线程记录
    pub(crate) worker_pool: ManageWorkerPool,
    /// simink 管理
    pub(crate) simink: ManageSimink,
    /// 命令行监视器管理
    pub(crate) readline: ManageReadline,
    /// 虚机管理
    pub(crate) guest: ManageGuest,
}

impl SiminkManage {
    /// 创建一个 simink 管理器
    pub fn new(sys: Arc<DfsInode>, rcu: ThreadId) -> Self {
        Self {
            worker_pool: ManageWorkerPool::new(sys, rcu),
            simink: ManageSimink::new(),
            readline: ManageReadline::new(),
            guest: ManageGuest::new(),
        }
    }
}
