use std::sync::Mutex;

use crate::SiminkManage;

/// guest 运行状态
#[derive(Debug, Clone, Copy)]
pub enum ManageGuestStatus {
    /// 虚机不存在,
    GuestNone,
    /// 虚机预配置
    GuestPreConfig,
    /// 虚机配置
    GuestConfig,
    /// 虚机被配置
    GuestConfiged,
    /// 虚机运行中
    GuestRunning,
    /// 虚机被 traced(GDB)
    GuestTraced,
    /// 虚机暂停
    GuestPaused,
    /// 虚机关机
    GuestShutdown,
    /// 虚机复位
    GuestReset,
    /// 虚机 panic
    GuestPanic,
}

impl ManageGuestStatus {
    /// 客户机状态人类可读形式
    pub fn name(&self) -> &'static str {
        match self {
            ManageGuestStatus::GuestNone => "No board",
            ManageGuestStatus::GuestPreConfig => "PreConfig",
            ManageGuestStatus::GuestConfig => "Config",
            ManageGuestStatus::GuestConfiged => "Configed",
            ManageGuestStatus::GuestRunning => "Running",
            ManageGuestStatus::GuestTraced => "Traced",
            ManageGuestStatus::GuestPaused => "Paused",
            ManageGuestStatus::GuestShutdown => "Shutdown",
            ManageGuestStatus::GuestReset => "Reset",
            ManageGuestStatus::GuestPanic => "Panic",
        }
    }
}

pub(crate) struct ManageGuest {
    status: Mutex<ManageGuestStatus>,
}

impl ManageGuest {
    pub(crate) fn new() -> Self {
        Self { status: Mutex::new(ManageGuestStatus::GuestNone) }
    }
}

impl SiminkManage {
    /// 获取客户机状态
    #[allow(clippy::missing_panics_doc)]
    pub fn guest_status(&self) -> ManageGuestStatus {
        let lock = self.guest.status.lock().unwrap();
        *lock
    }

    /// 设置 guest 状态
    #[allow(clippy::missing_panics_doc)]
    pub fn set_guest_status(&self, status: ManageGuestStatus) {
        let mut lock = self.guest.status.lock().unwrap();
        *lock = status;
    }
}
