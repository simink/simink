use std::sync::Arc;

use monitor::{MonError, Monitor, MonitorCommand};

struct Clear {
    mon: Arc<Monitor>,
}

impl MonitorCommand for Clear {
    fn mon_protocol_exec(&self, _: &monitor::Protocol) -> monitor::MonResult<monitor::Protocol> {
        Err(MonError::PermProtocol)
    }
    fn mon_readline_exec(
        &self,
        _: &std::collections::HashMap<String, String>,
        printer: &monitor::MonitorPrinter,
    ) -> monitor::MonResult<()> {
        if printer.is_main_mon() {
            self.mon.clear_scrollback().map_err(|_| MonError::SystemError)?;
        }
        Ok(())
    }

    fn mon_readline_help(&self) -> String {
        String::from("清屏")
    }
}

pub(crate) fn monitor_command_clear_register(mon: Arc<Monitor>) {
    let clear = Arc::new(Clear { mon: mon.clone() });
    mon.register_command("clear", clear).unwrap();
}
