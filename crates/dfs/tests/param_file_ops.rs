use std::sync::{
    atomic::{AtomicU32, Ordering},
    Arc, Mutex,
};

use dfs::{dfs_read, inode::inode_root, params::ParamCreateOptions};

#[test]
fn param_create_file() {
    let data = Arc::new(AtomicU32::new(2));
    ParamCreateOptions::new()
        .read_write(true)
        .name("test1")
        .param_u32(inode_root(), data.clone())
        .unwrap();

    let mut buf = String::new();
    let _ = dfs_read("/test1", &mut buf).unwrap();
    assert_eq!(buf, "2");

    data.store(55, Ordering::Relaxed);
    let mut buf = String::new();
    let _ = dfs_read("/test1", &mut buf).unwrap();
    assert_eq!(buf, "55");

    inode_root().remove_file("test1").unwrap();

    let data = Arc::new(Mutex::new(String::from("value")));
    ParamCreateOptions::new()
        .read_write(true)
        .name("test1")
        .param_string(inode_root(), data)
        .unwrap();
    let mut buf = String::new();
    let _ = dfs_read("/test1", &mut buf).unwrap();
    assert_eq!(buf, "value");
    inode_root().remove_file("test1").unwrap();
}
