use std::sync::Arc;

use manage::{SiminkManage, SiminkRequest};

/// 主程序循环处理系统事件
pub fn main_loop(manage: Arc<SiminkManage>) -> Arc<SiminkManage> {
    loop {
        let req = manage.simink_wait_request();
        if req == SiminkRequest::ReqHostExit {
            break;
        }
    }
    manage
}
