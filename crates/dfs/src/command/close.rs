use std::{collections::HashMap, sync::Arc};

use monitor::{CompleterSuggestion, Monitor, MonitorCommand};

use super::fdinfo::{fdinfo_remove, mon_fd_completer};
use crate::{DfsError, DfsResult};

struct CloseCommand;

impl CloseCommand {
    fn do_close(&self, args: &HashMap<String, String>) -> DfsResult<()> {
        let file = args.get("fd").unwrap();
        let id = file.parse::<u64>().map_err(|_| DfsError::InvalidArgs)?;
        fdinfo_remove(id)
    }
}

impl MonitorCommand for CloseCommand {
    fn mon_protocol_exec(&self, args: &monitor::Protocol) -> DfsResult<monitor::Protocol> {
        self.do_close(args)?;
        let hash = HashMap::new();
        Ok(hash)
    }

    fn mon_readline_arg_type(&self) -> Option<&'static str> {
        Some("fd:u64")
    }

    fn mon_readline_completer(&self, args: &[&str], is_space: bool) -> Option<CompleterSuggestion> {
        mon_fd_completer(args, is_space)
    }
    fn mon_readline_exec(
        &self,
        args: &HashMap<String, String>,
        _: &monitor::MonitorPrinter,
    ) -> DfsResult<()> {
        self.do_close(args)
    }

    fn mon_readline_help(&self) -> String {
        String::from("关闭一个文件")
    }

    fn mon_readline_long_help(&self) -> Option<String> {
        Some(String::from("close <fd>   - 关闭从命令行或者控制协议打开的文件"))
    }
}

pub(crate) fn monitor_command_close_register(mon: &Monitor) {
    mon.register_command("close", Arc::new(CloseCommand)).unwrap();
}
